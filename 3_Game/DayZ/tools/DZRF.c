modded class DayZGame {
	protected ref map<string, string> DZRF_ServerConfig;
	protected string globalRootDir;
	
	bool isDiagServer;
	bool isDiagMPClient;
	
	void DayZGame()
	{
		DZRF_ServerConfig = new map<string, string>;
		
		//taken from VPPAdminTools
		isDiagServer 		 = GetGame().IsMultiplayer() && GetGame().IsDedicatedServer();   //Diag server
		isDiagMPClient		 = GetGame().IsMultiplayer() && !GetGame().IsDedicatedServer();  //Diag client 
		//taken from VPPAdminTools
	}
	
	
	void DZRF_AddToServerCfg(string d_filename, string d_value)
	{
		if (isDiagServer)
		{
			Print(DZRF_ServerConfig);
			DZRF_ServerConfig.Insert(d_filename, d_value);
			Print("[dzr_framework] ::: DZRF AddToServerCfg: "+d_filename+ " > " + DZRF_ServerConfig.Get(d_filename) );
		}
	}
	
	
	void DZRF_SaveClientCfg(string d_filename, string d_value)
	{
		if (isDiagMPClient)
		{
			Print(DZRF_ServerConfig);
			DZRF_ServerConfig.Insert(d_filename, d_value);
			Print("[dzr_framework] ::: DZRF AddToServerCfg isDiagMPClient: "+d_filename+ " > " + DZRF_ServerConfig.Get(d_filename) );
		}
	}
	
	
	string DZRF_getCfg(string d_filename)
	{
		if (isDiagServer)
		{
			Print(DZRF_ServerConfig);
			Print("[dzr_framework] ::: DZRF getCfg: "+d_filename+ " > " + DZRF_ServerConfig.Get(d_filename) );
			return DZRF_ServerConfig.Get(d_filename);
		}
		return "isDiagServer ERROR";
	}
	
	void DZRF_SetGlobalRoorDir(string rootDir = "$profile:\\DZR\\CfgDemo")
	{
		globalRootDir = rootDir;
	}
	
	void DZRF_UpdateConfigsGlobally()
	{
		DZRF().UpdateConfigsGlobally(globalRootDir);
	}
	
	void DZRF_CreateConfig(string dfileName, string dValue)
	{
		DZRF().GetFile(globalRootDir, dfileName, dValue, DZR_IO_Command.GET, "", true);
	}
	
	string DZRF_ReadConfig(string dfileName)
	{
		string dResult = "ERROR";
		if ( isDiagMPClient )
		{
			dResult = DZRF_ServerConfig.Get(dfileName);
			Print( "-----------------isDiagMPClient------------DZRF_ReadConfig: "+dfileName );
			Print( dResult );
			//return DZRF().GetFile(globalRootDir, dfileName);
		}
		else
		{
			dResult = "Not client?";
			Print(globalRootDir +" : "+ dfileName);
			return DZRF().GetFile(globalRootDir, dfileName);
		}
		return dResult;
	}
	
}


enum DZR_IO_Command
{
	GET			= 0,
	DELETE			= 1,
	APPEND			= 2,
}
enum DZR_IO_Flag
{
	GET			= 0,
}


class DZRF
{
	//ref map<string, string> ServerConfig;
	bool isDiagServer;
	bool isDiagMPClient;
	
	void DZRF()
	{
		// Print("[dzr_framework] ::: DZRF init");
		// ServerConfig = new map<string, string>;
		// ServerConfig.Insert("INIT", "0");
		
		//taken from VPPAdminTools
		isDiagServer 		 = GetGame().IsMultiplayer() && GetGame().IsDedicatedServer();   //Diag server
		isDiagMPClient		 = GetGame().IsMultiplayer() && !GetGame().IsDedicatedServer();  //Diag client 
		//taken from VPPAdminTools
		
		
		//AddToGlobalConfig("globalCfg.txt", "this is added from server");
		
	}
	
	void AddToGlobalConfig(string d_key, string d_value)
	{
		if(isDiagServer)
		{
			GetDayZGame().DZRF_AddToServerCfg(d_key,d_value);
			ref Param2<string, string> param = new Param2<string,string>(d_key,d_value);
			GetRPCManager().SendRPC( "DZR_FRM", "DZR_AddToGlobalConfigOnClient", param, true);
		}
	}
	
	
	string GetServerFile(string folderPath, string fileName, string newContents = "", int IO_Command = DZR_IO_Command.GET)
	{
		if(isDiagServer)
		{
			return GetFile( folderPath,  fileName,  newContents ,  IO_Command ,  "Server");
		}
		else
		{
			Print("[dzr_framework] ::: GetServerFile cannot be called from client");
			return "[dzr_framework] ::: GetServerFile cannot be called from client";
		}
	}
	
	
	string GetClientFile(string folderPath, string fileName, string newContents = "", int IO_Command = DZR_IO_Command.GET )
	{
		if(isDiagMPClient)
		{
			return GetFile( folderPath,  fileName,  newContents ,  IO_Command , "Client");
		}
		else
		{
			Print("[dzr_framework] ::: GetClientFile cannot be called from server");
			return "[dzr_framework] ::: GetClientFile cannot be called from server";
		}
	}
	
	string GetFile(string folderPath, string fileName, string newContents = "", int IO_Command = DZR_IO_Command.GET, string execSide = "Both", bool NoChangeIfExists = 0 )
	{
		string m_TxtFileName = fileName;
		string fileContent;
		FileHandle fhandle;
		string defaultContents;
		string pth = folderPath +"/"+ m_TxtFileName;
		
		if ( FileExist(folderPath +"/"+ m_TxtFileName) && (IO_Command == DZR_IO_Command.GET || IO_Command == DZR_IO_Command.APPEND ) )
		{
			
			
			//file
			fhandle	=	OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.READ);
			//string server_id;
			FGets( fhandle,  fileContent );
			CloseFile(fhandle);
			
			if(!NoChangeIfExists)
			{
				if(newContents != "")
				{
					if(newContents != fileContent)
					{
						fileContent = newContents;
						
						if( IO_Command == DZR_IO_Command.APPEND)
						{
							fhandle = OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.APPEND);
						}
						else 
						{
							fhandle = OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.WRITE);
						};
						
						FPrintln(fhandle, fileContent);
						CloseFile(fhandle);
					};
					
					Print("[dzr_framework] "+execSide+" :::  File "+folderPath +"/"+ m_TxtFileName+" Updated to: "+fileContent);
				}
			}
			
			Print("[dzr_framework] "+execSide+" :::  File "+folderPath +"/"+ m_TxtFileName+" is OK! Contents: "+fileContent);
			AddToGlobalConfig(m_TxtFileName, fileContent);
			return fileContent;
		}
		else 
		{
			// checking dirs
			TStringArray parts();
			string path = folderPath;
			path.Split("/", parts);
			path = "";
			foreach (string part: parts)
			{
				path += part + "/";
				if (part.IndexOf(":") == part.Length() - 1)
				continue;
				
				if(IO_Command == DZR_IO_Command.GET)
				{
					if (!FileExist(path) && !MakeDirectory(path))
					{
						Print("Could not make dirs from path: " + path);
						return "Could not make dirs from path: " + path;
					}
				}
				
				if(IO_Command == DZR_IO_Command.DELETE)
				{
					DeleteFile(pth);
					Print("Deleted file: "+pth);
					return "Deleted file: "+pth;
				}
			}
			
			//Default new file
			FileHandle file = OpenFile(pth, FileMode.WRITE);
			if(newContents == "")
			{
				newContents = "You tried to create a file using dzr_framework, but did not provide file contents. So this default content is added.";
			}
			if (file != 0 && IO_Command == DZR_IO_Command.GET)
			{
				FPrintln(file, newContents);
			CloseFile(file);
			//file
			fhandle	=	OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.READ);
			//string server_id;
			FGets( file,  fileContent );
			
			Print("[dzr_framework] "+execSide+" :::  File "+folderPath +"/"+ m_TxtFileName+" is OK! Contents: "+fileContent);
			CloseFile(fhandle);
			AddToGlobalConfig(m_TxtFileName, fileContent);
			return fileContent;
			}
		}
		Print("[dzr_framework] isDiagServer:"+isDiagServer+" isDiagMPClient:"+isDiagMPClient+" :::  File "+folderPath +"/"+ m_TxtFileName+" ERROR!");
		return "[dzr_framework] isDiagServer:"+isDiagServer+" isDiagMPClient:"+isDiagMPClient+" :::  File "+folderPath +"/"+ m_TxtFileName+" ERROR!";
	}
	
	void UpdateConfigsGlobally(string rootDir)
	{
		//iterate all cfg folder from server
		string cfgFolder = rootDir;
		TStringArray ServerFiles = GetFoldersList(cfgFolder);
		
		foreach (string ServerFile : ServerFiles)
		{
			//send each as rpc
			string dirName = cfgFolder;
			string fileName = ServerFile;
			string fileNameWex;
			string path = dirName +"/"+ fileName;
			
			TStringArray array_name = new TStringArray;
			fileName.Split( ".", array_name );
			//Print(array_name);
			int array_name_entries_count = array_name.Count();
			//Print(array_name_entries_count);
			string fileContent = DZRF().GetFile(dirName, fileName);
			fileNameWex = array_name.Get(array_name_entries_count - 2);
			//Print(fileNameWex);
			ref Param2<string,string> m_Data = new Param2<string,string>(fileNameWex, fileContent);
			GetRPCManager().SendRPC( "DZR_FRM", "DZR_FileContentsToClient", m_Data, true );
		}
		
	}
	
	
	
	private TStringArray GetFoldersList(string m_PathToFolders)
	{
		////Print("GetFoldersList");
		string	file_name;
		int file_attr;
		int		flags;
		TStringArray list = new TStringArray;
		
		
		
		string path_find_pattern = m_PathToFolders+"\\*"; //*/
		FindFileHandle file_handler = FindFile(path_find_pattern, file_name, file_attr, flags);
		
		bool found = true;
		while ( found )
		{				
			if ( file_attr == FileAttr.DIRECTORY )
			{
				////Print("GetFoldersList IS DIR: "+file_name);
				list.Insert(file_name);
			}
			
			found = FindNextFile(file_handler, file_name, file_attr);
		}
		
		return list;
	}
	
}