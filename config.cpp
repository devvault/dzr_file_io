class CfgPatches
{
	class dzr_framework
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {};
		weapons[] = {};
	};
};

class CfgMods
{
	class dzr_framework
	{
		type = "mod";
		author = "";
		description = "";
		dir = "dzr_framework";
		name = "dzr_framework";
		//inputs = "dzr_framework/Data/Inputs.xml";
		dependencies[] = {"Core", "Game", "World", "Mission"};
		class defs
		{
			class engineScriptModule
			{
				files[] = {"dzr_framework/Common",  "dzr_framework/1_Core"};
			};
			class gameLibScriptModule
			{
				files[] = {"dzr_framework/Common",  "dzr_framework/2_GameLib"};
			};
			class gameScriptModule
			{
				files[] = {"dzr_framework/Common",  "dzr_framework/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_framework/Common", "dzr_framework/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_framework/Common", "dzr_framework/5_Mission"};
			};

		};
	};
};